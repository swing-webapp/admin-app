process.env.VUE_APP_VERSION = require('./package.json').version
const now = new Date(Date.now())
process.env.VUE_APP_BUILDTIME= now.toLocaleDateString("en-GB", {timeZone: "Europe/Zurich",year: "numeric", month: "long", day: "numeric"})
module.exports = {
  configureWebpack: {
    devServer: {
      headers: { "Access-Control-Allow-Origin": "*" }
    }
  },
  publicPath: process.env.NODE_ENV === 'production'
      ? process.env.APP_BASEPATH
      : '/',
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  transpileDependencies: [
    'quasar'
  ]
}
