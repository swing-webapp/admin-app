import VueJwtDecode, {decode} from "jsonwebtoken";
import router from "@/router";
const state = {
    user: null,
    token: null,
};

const getters = {
    isAuthenticated: (state) => !!state.token&&state.user.admin,
    user: (state) => state.user,
    id:(state)=>state.id,
    isTokenValid(state) {
        try {
            const date = new Date(0);
            const decoded = decode(state.token);
            date.setUTCSeconds(decoded.exp);
            return date.valueOf() > new Date().valueOf();
        } catch (err) {
            return false;
        }
    },
};

const actions = {
    async LogIn({commit}, token) {
        const userinfo = VueJwtDecode.decode(token);
        await commit("setToken", token);
        await commit("setUser",userinfo);

    },
    async LogOut({ commit }) {
        await commit("setToken", null);
        await commit("setUserlogout");
        await router.push({name:"Login"});

    },
};
const mutations = {
    setToken(state, token) {
        if(!token){
            localStorage.removeItem("eawag-token-admin");
        } else {
            localStorage.setItem("eawag-token-admin", token);
        }
        state.token = token;
    },
    setUser(state,userinfo){
      state.user=userinfo;
      state.id=userinfo.id;
    },
    setUserlogout(state){
        state.user=null;
        state.id=null;
    },

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};

