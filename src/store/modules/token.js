import {api} from "@/helpers/api";
import {Notify} from "quasar";

const state = {
    editsurvey:{
        tokens:null,
        test:null,
        survey_id:null,
        short_name:null,
    },
    loading:false,
    globaltoken:[],
    global:true,
    status:false
}

const getters = {
    getList:(state)=>{
        if(state.global) return state.globaltoken
        return state.editsurvey.tokens
    },
    getStatus:(state)=>{
        return state.status
    },
    }

const actions = {
    async getTokenList({dispatch,commit},{global,survey_id='',short_name=''}){
        commit('setGlobal',global)
        commit('setSurveyId',{survey_id:survey_id,short_name:short_name})
        if(global){
            await dispatch('getGlobalTokenList')
        } else {
            await dispatch('getSurveyToken',survey_id)
        }
    },
    async updateTokenList({state,dispatch}){
    if(state.global){
        await dispatch('getGlobalTokenList')
    } else {
        await dispatch('getSurveyToken',state.editsurvey.survey_id)
    }
},
    async getSurveyToken({commit},survey_id) {
        await commit("setLoading",true)
        const payload = await api.listsurveytoken(survey_id);
        if (payload.status==="OK") {
            commit('setSurveyToken', payload.tokens)
        }
        else {
            commit('setSurveyToken', [])
        }
        await commit("setLoading",false)
    },
    async getSurveyTest({commit},survey_id) {
        const payload = await api.testsurveytoken(survey_id);
        if (payload.status==="OK") {
            commit('setSurveyTokenTest', payload.tokens.token)
        }
        else {
            commit('setSurveyToken', '')
        }
    },
    async getGlobalTokenList({commit}){
        await commit("setLoading",true)
        const payload = await api.listtoken();
        if (payload.status==="OK") {
            commit('setTokenList', payload.tokens)
        }
        else {
            commit('setTokenList', [])
        }
        await commit("setLoading",false)
    },

    async downSurveyJSON({state}){
        await api.downloadtokenJSON(state.editsurvey.survey_id,state.editsurvey.short_name);

    },
    async downSurveyCSV({state}){
        await api.downloadtokenCSV(state.editsurvey.survey_id,state.editsurvey.short_name);

    },
    async addToken({dispatch,commit},form){
        commit('setStatus',false)
        form.status="new";
        const payload = await api.registertoken(form);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Token created"

            });
            commit('setStatus',true)
            dispatch('updateTokenList');
        }
    },
    async addTokensBatch({dispatch,commit},{tab,advanced,advform}){
        commit('setStatus',false)
        commit('setLoading',true)

        let oks=0;
        for(let el of tab) {
            if(el.id) {
                let form = {}

                if (advanced){
                    form=advform
                    form.advanced=true
                }else {
                    form.status = "new";
                }
                form.token = el.token
                form.survey_id = el.id

                const payload = await api.registertoken(form);
                if (payload && payload.status === "OK") {
                    oks++;
                    el.status = "OK"
                } else {
                    el.status = "ERROR : " + payload.message
                }
            }else {
                el.status = "ERROR : Survey Unknown"
            }
        }
        if (oks===tab.length) {
            Notify.create({
                type: 'positive',
                message: oks+ " Token(s) Created"
            });
            commit('setStatus',true)

        }else{
            Notify.create({
                type: 'warning',
                message: oks+ " Token(s) Created and " +(tab.length-oks)+" Error(s)"
            });
        }
        commit('setLoading',false)
        dispatch('updateTokenList');
        return tab
    },
    async updateToken({dispatch,commit},{id,form}){
        commit('setStatus',false)
        const payload = await api.updatetoken(form,id);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Token Updated"

            });
            commit('setStatus',true)
            dispatch('updateTokenList');
        }
    },

    async deleteToken({dispatch},id){
        const payload = await api.removetoken(id);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Token Removed"

            });
            dispatch('updateTokenList');
        }
    },
    async deleteTokens({dispatch},ids){
        let oks=0;
        for(let x in ids) {
            const payload = await api.removetoken(ids[x]);
            if (payload && payload.status === "OK") {
                oks++;
            }
        }
        if (oks===ids.length) {
            Notify.create({
                type: 'positive',
                message: oks+ " Token(s) Removed"
            });
        }else{
            Notify.create({
                type: 'warning',
                message: oks+ " Token(s) Removed and " +(ids.length-oks)+" Error(s)"
            });
        }
        dispatch('updateTokenList');
    },
    async getToken({dispatch},id){
        dispatch('updateTokenList');
        const payload = await api.gettoken(id);
        if (payload.status==="OK"&&payload.token){
            this.form=payload.token;
            return payload.token
        }

    },

    async changeEnableTokens({dispatch},{ids,state}){
        let oks=0;
        for(let x in ids){
            const id = ids[x];
            let payload
            let text=''

            if (state){
                payload = await api.enabletoken(id);
                text = "Enabled"
            } else {
                payload = await api.disabletoken(id);
                text= "Disabled"

            }

            if (payload && payload.status === "OK") {
                oks++;
            }


            if (oks===ids.length) {
                Notify.create({
                    type: 'positive',
                    message: oks+ " Token(s) "+text
                });
            }else{
                Notify.create({
                    type: 'warning',
                    message: oks+ " Token(s) "+text+" and " +(ids.length-oks)+" Error(s)"
                });
            }
        }
        dispatch("updateTokenList")
    },

}
const mutations = {
    setStatus(state,status){
        state.status=status
    },
    setSurveyToken(state, tokens) {
        state.editsurvey.tokens = tokens;
    },
    setTokenList(state, tokens) {
        state.globaltoken = tokens;
    },
    setSurveyTokenTest(state, token) {
        state.editsurvey.test = token;
    },
    setLoading(state, loading) {
        state.loading = loading;
    },
    setGlobal(state,global){
        state.global=global;
    },
    setSurveyId(state,{survey_id,short_name}){
        state.editsurvey.survey_id=survey_id;
        state.editsurvey.short_name=short_name;

    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
