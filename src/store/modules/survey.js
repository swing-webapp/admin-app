import {api} from '@/helpers/api';
import {Loading, Notify,Dialog} from "quasar";
import DispValidation from '@/components/DispValidation'

const state = {
    surveys:[],
    survey:null,
    loading:false,
    translist:[],
    trans:[],
    templatevar:[],
    status:null,
    globalopt:{},
    editsurvey:{
        id:null,
        origin_data:null,
        updated_data:null,
    }

};

const getters = {
    getQuestion:(state)=>(pre,id) =>{
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey
        } else {
            survey=state.editsurvey.updated_data.postsurvey
        }
        const quest = survey.find(o => o._id === id);

        return JSON.parse(JSON.stringify(quest))
    },
    getAlternative:(state)=>(id) =>{
        const alt = state.editsurvey.updated_data.rational.alternatives.items.find(o => o._id === id);
        return JSON.parse(JSON.stringify(alt))
    },
    getAlternativeDes:(state) =>{
        const des=state.editsurvey.updated_data.rational.alternatives.content
        return JSON.parse(JSON.stringify(des))
    },
    getRationalObj:(state)=>{
        const gen=state.editsurvey.updated_data.rational.objectives.items
        return JSON.parse(JSON.stringify(gen))
    },
    getObjectiveDes:(state) =>{
        const des=state.editsurvey.updated_data.rational.objectives.content
        return JSON.parse(JSON.stringify(des))
    },
    getRationalGen:(state)=>{
        const gen=state.editsurvey.updated_data.rational.general
        return JSON.parse(JSON.stringify(gen))
    },

    getGlobalPOC:function(state){
        let poc=state.globalopt.poc;
        delete poc.useglobal;
        return poc
    },
    getGlobal:function(state){
        if(Object.entries(state.globalopt).length!==0) return state.globalopt
        return {poc:{address:{}}}
    },
    getUpdateSurvey:(state)=>(field)=>{
        return state.editsurvey.updated_data[field]
    },
    getIdtoSn:function(state){
        let IdtoSn = {}
        state.surveys.forEach(survey=>IdtoSn[survey.id]=survey.general.short_name);
        return IdtoSn
    },
    getSntoId:function(state){
        let SntoId = {}
        state.surveys.forEach(survey=>SntoId[survey.general.short_name]=survey.id);
        return SntoId
    },
    getSelectListSurveys(state){
        let list=[]
        state.surveys.forEach(
            (survey)=>
            list.push({
                label:survey.general.short_name,
                value:survey.id,
                description:survey.general.description,
            })
        )
        return list
        },
    getTableSurveys(state){
        let list=[]
        state.surveys.forEach(
            (survey)=>
                list.push({
                    id:survey.id,
                    short_name:survey.general.short_name,
                    description:survey.general.description,
                    status:survey.status,
                    createdAt:survey.createdAt,
                    enable:survey.general.enable
                })
        )
        return list
    },

    getSelectListLanguages(state){
        let list=[]
        state.translist.forEach(
            (trans)=>
                list.push({
                    label:trans.language,
                    value:trans.language,
                    icon:'flag:'+trans.flag,
                })
        )
        return list
    },
    getTableQuestions:(state)=>(pre) =>{
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey
        } else {
            survey=state.editsurvey.updated_data.postsurvey
        }
        let list=[]
        survey.forEach(
            (quest)=>
                list.push({
                    id:quest._id,
                    short_name:quest.short_name,
                    description:quest.description,
                    type:quest.type,
                    comments:quest.comments,
                    enable:quest.enable

                })
        )
        return list
    },
    getTableAlternatives:(state) => {
        let list = []
        const survey=state.editsurvey.updated_data.rational.alternatives.items
        survey.forEach(
            (alt) =>
                list.push({
                    id: alt._id,
                    short_name: alt.short_name,
                    description: alt.description,
                    comments: alt.comments,
                    enable: alt.enable
                })
        )
        return list
    },
    getListObj:(state)=>{
        let objlist = [];
        state.editsurvey.updated_data.rational.objectives.items.forEach((key) => {
            if(key.type==='objective') objlist.push({label:key.short_name,value:key._id});
            if(key.children) {
                key.children.forEach((key) => {
                    if(key.type==='objective') objlist.push({label:key.short_name,value:key._id});
                })
            }})
        return objlist
    },
    getListAltandObj:(state)=>{
        let altlist = []
        state.editsurvey.updated_data.rational.alternatives.items.forEach(
            (alt) => altlist.push(alt.short_name))
        let objlist = [];
        state.editsurvey.updated_data.rational.objectives.items.forEach((key) => {
            if(key.type==='objective') objlist.push(key.short_name);
            if(key.children) {
                key.children.forEach((key) => {
                    if(key.type==='objective') objlist.push(key.short_name);
                })
            }})
        return {objectives:objlist,alternatives:altlist}
    },
    getPredicMat:(state)=>{
        const gen=state.editsurvey.updated_data.rational.predicmat || {}
        return gen
    },
};

const actions = {
    async initPredicmat({commit,getters}){
        const list = getters['getListAltandObj']
        const storemat = getters['getPredicMat']
        let newmat={};
        list.alternatives.forEach(alt=>{
            if(!newmat[alt]) newmat[alt]={}
            list.objectives.forEach(obj=>{
                if(storemat[alt]&&storemat[alt][obj]){
                    newmat[alt][obj]=storemat[alt][obj];
                } else {newmat[alt][obj]=0;}
            })
        })
        commit('putPredicMat',newmat)
    },
    async addAlternative({commit,state}, list) {
        list.short_name = list.short_name.toUpperCase().replace(/ /g, '');
        if (!state.editsurvey.updated_data.rational.alternatives.items.find(o => o.short_name === list.short_name)) {
            await commit('addAlt', list)
            Notify.create({
                type: 'positive',
                message: "Alternative created"
            });
            await commit("setStatus",true);

        } else {
            Notify.create({
                type: 'warning',
                message: "Id already exist"
            });
            await commit("setStatus",false);

        }
    },
    async reorderAlternative({commit,state},tab){
        const survey = state.editsurvey.updated_data.rational.alternatives.items
        let items=[]
        for (let i in tab){
            const el = tab[i];
            items.push(survey.find(o=>o.short_name===el.short_name))
        }
        commit('updateAlternativeItems',items)


    },
    async updateAlternative({commit,state},{list,aid}){
        const survey = state.editsurvey.updated_data.rational.alternatives.items
        let updated = false;
        for(let i in survey) {
            let o = survey[i]
            if (o._id === aid) {
                await commit('updateAlt', {index: i, list: list})
                updated = true;
                break;
            }
        }
        if(updated){
            Notify.create({
                type: 'positive',
                message: "Alternative updated"
            });
            await commit("setStatus",true);
        } else {
            Notify.create({
                type: 'warning',
                message: "Cannot update"
            });
            await commit("setStatus",false);
        }
    },
    async deleteAlternative({commit,state},id){
        let survey = state.editsurvey.updated_data.rational.alternatives.items

        const removed=survey.find((o, i) => {
            if (o._id === id) {
                commit('rmAlt', i);
                return true; // stop searching
            }
        })
        if(removed){
            Notify.create({
                type: 'positive',
                message: "Alternative Removed"})
        } else {
            Notify.create({
                type: 'warning',
                message: "Cannot remove alternativee"})
        }
    },
    async deleteAlternatives({commit,state},ids){
        let survey = state.editsurvey.updated_data.rational.alternatives.items

        let oks=0;

        for(let i in ids) {
            let id = ids[i]
            const removed=survey.find((o, i) => {
                if (o._id === id) {
                    commit('rmAlt', i);
                    return true; // stop searching
                }
            })
            if(removed) oks++;
        }

        if(oks===ids.length){
            Notify.create({
                type: 'positive',
                message: oks+ " Alternative(s) Removed"})
        } else {
            Notify.create({
                type: 'warning',
                message: oks+ " Alternative(s) Removed and " +(ids.length-oks)+" Error(s)"})
        }
    },
    async changeEnableAlternatives({commit,state},{ids,qstate}){
        let survey= state.editsurvey.updated_data.rational.alternatives.items
        let oks=0;

        for(let i in ids) {
            let id = ids[i]
            const find=survey.find((o, i) => {
                if (o._id === id) {
                    commit('changeEnableAlt', {index:i,qstate:qstate});
                    return true; // stop searching
                }
            })
            if(find) oks++;
        }
        let text=''
        if(qstate){
            text = "Enabled"
        } else {
            text= "Disabled"
        }
        if(oks===ids.length){
            Notify.create({
                type: 'positive',
                message: oks+ " Alternative(s) "+ text})
        } else {
            Notify.create({
                type: 'warning',
                message: oks+ " Alternative(s) " + text + " and " +(ids.length-oks)+" Error(s)"})
        }
    },
    async update_objectives({commit,state},{id,vari,val}){
        const survey = state.editsurvey.updated_data.rational.objectives.items;
        survey.find((o, i) => {
            if (o._id === id) {
               commit ('updateObjectiveItemVar',{index:i,vari:vari,val:val});
               return true; // stop searching
            }

            if(o.children) {
                let rco = null
                rco = o.children.find((co, ci)=>{
                    if (co._id === id) {
                       commit ('updateObjectiveChildren',{index:i,indexc:ci,vari:vari,val:val})
                       return true; // stop searching
                    }
                if(rco) {return true}
                });

            }
        
        });

    },
    async addPrePostSurvey({commit,state}, {pre, list}) {
        let survey
        if (pre) {
            survey = state.editsurvey.updated_data.presurvey
        } else {
            survey = state.editsurvey.updated_data.postsurvey
        }
        list.short_name = list.short_name.toUpperCase().replace(/ /g, '');
        if (!survey.find(o => o.short_name === list.short_name)) {
            if(pre) {
                await commit('addPreQuest', list)
            } else {
                await commit('addPostQuest', list)
            }
            Notify.create({
                type: 'positive',
                message: "Question created"
            });
            await commit("setStatus",true);

        } else {
            Notify.create({
                type: 'warning',
                message: "Id already exist"
            });
            await commit("setStatus",false);

        }
    },
    async updatePrePostSurvey({commit,state},{pre,list,qid}){
        let survey
        if (pre) {
            survey = state.editsurvey.updated_data.presurvey
        } else {
            survey = state.editsurvey.updated_data.postsurvey
        }
        let updated = false;
        for(let i in survey) {
            let o = survey[i]
            if (o._id === qid) {

                if (pre) {
                    await commit('updatePreQuest', {index: i, list: list})
                } else {
                    await commit('updatePostQuest', {index: i, list: list})
                }
                updated = true;
                break;
            }
        }

        if(updated){
            Notify.create({
                type: 'positive',
                message: "Question updated"
            });
            await commit("setStatus",true);
        } else {
            Notify.create({
                type: 'warning',
                message: "Cannot update"
            });
            await commit("setStatus",false);
        }
    },

    async reorderQuestions({commit,state},{pre,tab}){
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey;
        } else {
            survey=state.editsurvey.updated_data.postsurvey;
        }        let items=[];
        for (let i in tab){
            const el = tab[i];
            items.push(survey.find(o=>o.short_name===el.short_name));
        }
        if(pre){
            commit('updatePreSurvey',items);
        } else {
            commit('updatePostSurvey',items);
        }

    },
    async deleteQuestion({commit,state},{pre,id}){
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey
        } else {
            survey=state.editsurvey.updated_data.postsurvey
        }
        const removed=survey.find((o, i) => {
            if (o._id === id) {
                if (pre) {
                    commit('rmPreQuest', i);
                } else {
                    commit('rmPostQuest', i);
                }
                return true; // stop searching
            }
        })
        if(removed){
            Notify.create({
                type: 'positive',
                message: "Question Removed"})
        } else {
                Notify.create({
                    type: 'warning',
                    message: "Cannot remove question"})
            }
    },
    async deleteQuestions({commit,state},{pre,ids}){
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey
        } else {
            survey=state.editsurvey.updated_data.postsurvey
        }
        let oks=0;
        for(let i in ids) {
            let id = ids[i]
            const removed=survey.find((o, i) => {
                if (o._id === id) {
                    if (pre) {
                        commit('rmPreQuest', i);
                    } else {
                        commit('rmPostQuest', i);
                    }
                    return true; // stop searching
                }
            })
            if(removed) oks++;
        }
        if(oks===ids.length){
            Notify.create({
                type: 'positive',
                message: oks+ " Question(s) Removed"})
        } else {
            Notify.create({
                type: 'warning',
                message: oks+ " Question(s) Removed and " +(ids.length-oks)+" Error(s)"})
        }
    },
    async changeEnableQuestions({commit,state},{pre,ids,qstate}){
        let survey
        if(pre){
            survey=state.editsurvey.updated_data.presurvey
        } else {
            survey=state.editsurvey.updated_data.postsurvey
        }
        let oks=0;

        for(let i in ids) {
            let id = ids[i]
            const find=survey.find((o, i) => {
                if (o._id === id) {
                    if (pre) {
                        commit('changeEnablePreQuest', {index:i,qstate:qstate});
                    } else {
                        commit('changeEnablePostQuest', {index:i,qstate:qstate});
                    }
                    return true; // stop searching
                }
            })
            if(find) oks++;
        }
        let text=''
        if(qstate){
            text = "Enabled"
        } else {
            text= "Disabled"
        }
        if(oks===ids.length){
            Notify.create({
                type: 'positive',
                message: oks+ " Question(s) "+ text})
        } else {
            Notify.create({
                type: 'warning',
                message: oks+ " Question(s) " + text + " and " +(ids.length-oks)+" Error(s)"})
        }
    },
    async getAllSurveys({commit}) {
        await commit("setLoading",true)
        const payload = await api.listsurvey();
        if (payload.status==="OK") {
            commit('setSurveys', payload.surveys)
        }
        else {
            commit('setSurveys', [])
        }
        await commit("setLoading",false)
    },
    async getSurvey({commit,state},id) {
        if (id!==state.editsurvey.id){
             Loading.show({
                message: 'Loading data <br/><span class="text-primary">Hang on...</span>'
            });
            const payload = await api.getsurvey(id);
            if (payload.status==="OK") {
                commit('setEdit', payload.survey)
                Loading.show({
                    message: 'Data Loaded successfully <br/><span class="text-primary">Working on display now ...</span>'
                });
            }
            else {
                Loading.show({
                    message: 'Error loading data <br/><span class="text-primary">Please try again...</span>'
                });
                commit('setEdit', null)
            }
            Loading.hide()
            await commit("setLoading",false)
        }
    },
    async createSurvey({dispatch,commit},form){
        const payload = await api.createsurvey(form);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Survey Created"

            });
            await commit("setStatus",true);

        } else {
            await commit("setStatus",false);

        }
        dispatch('getAllSurveys');
    },
    async updateSurvey({dispatch,commit,state}){
        const payload = await api.updatesurvey(state.editsurvey.updated_data,state.editsurvey.id);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Survey Updated"

            });
            await commit("setStatus",true);


        } else {
            await commit("setStatus",false);

        }
        Dialog.create({
            component: DispValidation,
            input: payload.validation,
          })


        dispatch('getAllSurveys');
    },

    async deleteSurvey({dispatch},surveyid){
        const payload = await api.removesurvey(surveyid);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Survey Removed"

        });
            dispatch('getAllSurveys');
        }
    },
    async deleteSurveys({dispatch},ids){
        let oks=0;
        for(let x in ids) {
            const payload = await api.removesurvey(ids[x]);
            if (payload && payload.status === "OK") {
                oks++;
            }
        }
        if (oks===ids.length) {
            Notify.create({
                type: 'positive',
                message: oks+ " Survey(s) Removed"
            });
        }else{
            Notify.create({
                type: 'warning',
                message: oks+ " Survey(s) Removed and " +(ids.length-oks)+" Error(s)"
            });
        }
        dispatch('getAllSurveys');
    },
    async changeEnableSurveys({dispatch},{ids,state}){
        let oks=0;
        let message =""
        let payload = null
        for(let x in ids) {
            if (state) {
                 payload = await api.enablesurvey(ids[x]);
                 message = " Survey(s) Enabled"
            }else{
                 payload = await api.disablesurvey(ids[x]);
                 message = " Survey(s) Disabled"
            }
            if (payload && payload.status === "OK") {
                oks++;
            }
        }
        if (oks===ids.length) {
            Notify.create({
                type: 'positive',
                message: oks+ message
            });
        }else{
            Notify.create({
                type: 'warning',
                message: oks+ message+" and " +(ids.length-oks)+" Error(s)"
            });
        }
        dispatch('getAllSurveys');
    },
    async downSurvey({dispatch},{id,short_name}){
        Loading.show({
            message: 'Loading data <br/><span class="text-primary">Hang on...</span>'
        });
        await api.downloadsurvey(id,short_name);

        Loading.hide()

        dispatch('getAllSurveys');

    },


    async uploadSurvey({dispatch,commit},{file,newid}){
        Loading.show({
            message: 'Uploading data <br/><span class="text-primary">Hang on...</span>'
        });
      const payload =  await api.uploadsurvey(file,newid);
      Loading.hide();

      if (payload.status==="OK") {
        Notify.create({
            type: 'positive',
            message: "Survey Uploaded"

        });
        await commit("setStatus", true);

        }else{
            await commit("setStatus", false);
        }       
      Dialog.create({
        component: DispValidation,
        input: payload.validation,
      })
      dispatch('getAllSurveys');

    },

    async cloneSurvey({dispatch,commit},{id,new_id}){
        Loading.show({
            message: 'Loading data <br/><span class="text-primary">Hang on...</span>'
        });



        const payload = await api.clonesurvey(id,new_id);
        if (payload.status==="OK") {
                Notify.create({
                    type: 'positive',
                    message: "Survey Cloned"

                });
                await commit("setStatus", true);

        }else{
            await commit("setStatus", false);
        }
        Loading.hide()
        dispatch('getAllSurveys');


    },
    async getAllTranslation({commit}) {
        await commit("setLoading",true)
        const payload = await api.listtranslation();
        if (payload.status==="OK") {
            commit('setTransList', payload.langtrans)
        }
        else {
            commit('setTransList', [])
        }
        await commit("setLoading",false)
    },
    async getTranslation({commit},language) {
        await commit("setLoading",true)
        const payload = await api.gettranslation(language);
        if (payload.status==="OK") {
            commit('setTrans', payload.langtrans)
        }
        else {
            commit('setTransList', [])
        }
        await commit("setLoading",false)
    },
    async createTranslation({commit,dispatch},form) {
        const payload = await api.createtranslation(form);

        if (payload.status === "OK") {
            await commit("setStatus",true);
            Notify.create({
                type: 'positive',
                message: "Translation created"
            });
        }else {
            await commit("setStatus",false);

        }
        dispatch('getAllTranslation');
    },
    async updateTranslation({commit,dispatch},{id,form}) {
        const payload = await api.updatetranslation(form,id);

        if (payload.status === "OK") {
            await commit("setStatus",true);
            Notify.create({
                type: 'positive',
                message: "Translation updated"
            });
        }else {
            await commit("setStatus",false);

        }
        dispatch('getAllTranslation');
    },

    async deleteTranslation({dispatch},surveyid){
        const payload = await api.removetranslation(surveyid);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Translation Removed"

            });
            dispatch('getAllTranslation');
        }
    },
    async deleteTranslations({dispatch},ids){
        let oks=0;
        for(let x in ids) {
            const payload = await api.removetranslation(ids[x]);
            if (payload && payload.status === "OK") {
                oks++;
            }
        }
        if (oks===ids.length) {
            Notify.create({
                type: 'positive',
                message: oks+ " Translation(s) Removed"
            });
        }else{
            Notify.create({
                type: 'warning',
                message: oks+ " Translation(s) Removed and " +(ids.length-oks)+" Error(s)"
            });
        }
        dispatch('getAllTranslation');
    },

    async saveTranslation(){
        const payload = await api.savetranslation();
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable saved"
            });
        }
    },
    async loadTranslation({dispatch}){
        const payload = await api.loadtranslation();
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable loaded"
            });
        }
        dispatch('getAllTranslation');
    },
    async downTranslation(){
        await api.downloadtranslation();
    },

    async uploadTranslation({dispatch},form){
        Loading.show({
            message: 'Loading data <br/><span class="text-primary">Hang on...</span>'
        });
        const payload = await api.uploadtranslation(form);
        Loading.hide()

        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Translation file uploaded and activated"
            });
        }
        dispatch('getAllTranslation');
    },

    async getAllTemplateVar({commit}) {
        await commit("setLoading",true)
        const payload = await api.listtemplatevar();
        if (payload.status==="OK") {
            commit('setTemplateVar', payload.langdefs)
        }
        else {
            commit('setTemplateVar', [])
        }
        await commit("setLoading",false)
    },

    async deleteTemplateVar({dispatch},surveyid){
        const payload = await api.removetemplatevar(surveyid);
        if (payload.status==="OK") {
            Notify.create({
                type: 'positive',
                message: "Variable Removed"

            });
            dispatch('getAllTemplateVar');
        }
    },
    async deleteTemplateVars({dispatch},ids){
        let oks=0;
        for(let x in ids) {
            const payload = await api.removetemplatevar(ids[x]);
            if (payload && payload.status === "OK") {
                oks++;
            }
        }
        if (oks===ids.length) {
            Notify.create({
                type: 'positive',
                message: oks+ " Variable(s) Removed"
            });
        }else{
            Notify.create({
                type: 'warning',
                message: oks+ " Variable(s) Removed and " +(ids.length-oks)+" Error(s)"
            });
        }
        dispatch('getAllTemplateVar');
    },
    async saveTemplateVar(){
        const payload = await api.savetemplatevar();
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable saved"
            });
        }
    },
    async loadTemplateVar({dispatch}){
        const payload = await api.loadtemplatevar();
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable loaded"
            });
        }
        dispatch('getAllTemplateVar');
    },
    async downTemplateVar(){
        await api.downloadtemplatevar();
    },

    async uploadTemplateVar({dispatch},form){
        const payload = await api.uploadtemplatevar(form);
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "File uploaded and activated"
            });
        }
        dispatch('getAllTemplateVar');
    },
    async createTemplateVar({dispatch},form){
        const payload = await api.createtemplatevar(form);
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable created"
            });
        }
        dispatch('getAllTemplateVar');
    },
    async updateTemplateVar({dispatch},{id,form}){
         const payload = await api.updatetemplatevar(form,id);
        if (payload.status === "OK") {
            Notify.create({
                type: 'positive',
                message: "Variable updated"
            });
        }
        dispatch('getAllTemplateVar');
    },
    async getAllGlobalOpt({commit}){
        await commit("setLoading",true)
        const payload = await api.listsurveyglobalopt();
        if (payload.status==="OK") {
            commit('setGlobalOpt', payload.options)
        }
        else {
            commit('setGlobalOpt', {})
        }
        await commit("setLoading",false)
    },
    async updateGlobalOpt({dispatch,commit},form){
        const payload = await api.updatesurveyglobalopt(form);
        if (payload.status === "OK") {
            await commit("setStatus",true);
            Notify.create({
                type: 'positive',
                message: "General options updated"
            });
        }else {
            await commit("setStatus",false);

        }

        dispatch('getAllGlobalOpt');
    },
};

const mutations = {
    setSurveys(state, surveys) {
        state.surveys = surveys;
    },
    setEdit(state, payload) {
        if (payload) {
            state.editsurvey.id = payload.id;
            // state.editsurvey.origin_data = payload; //Save some RAM not needed for the moment
            state.editsurvey.updated_data = payload;
        } else {
            state.editsurvey.id = null;
            state.editsurvey.origin_data = null;
            state.editsurvey.updated_data = null;
        }
    },
    updateEdit(state, {field, value}) {
        state.editsurvey.updated_data[field] = value;
    },
    updateEditWelcome(state, {language, value}) {
        value.language = language;
        const obj = state.editsurvey.updated_data.welcome.find((o, i) => {
            if (o.language ===  null){
                state.editsurvey.updated_data.welcome.splice(i,1)
            }
            else if (o.language === language) {
                state.editsurvey.updated_data.welcome[i] = value;
                return true; // stop searching
            }
        });
        if (!obj && language !== null) { //create language if not found
            state.editsurvey.updated_data.welcome.push(value)
        }
    },
    setTransList(state, list) {
        state.translist = list;
    },
    setTrans(state, list) {
        state.trans = list;
    },
    setTemplateVar(state, list) {
        state.templatevar = list;
    },
    setLoading(state, loading) {
        state.loading = loading;
    },
    setStatus(state, status) {
        state.status = status;
    },
    setGlobalOpt(state, opt) {
        state.globalopt = opt;
    },

    rmPreQuest(state, index) {
        state.editsurvey.updated_data.presurvey.splice(index, 1);
    },
    rmPostQuest(state, index) {
        state.editsurvey.updated_data.postsurvey.splice(index, 1);
    },
    updatePreSurvey(state,survey){
        state.editsurvey.updated_data.presurvey=survey
    },
    updatePostSurvey(state,survey){
        state.editsurvey.updated_data.postsurvey=survey
    },
    addPreQuest(state,list){
        state.editsurvey.updated_data.presurvey.push(list)
    },
    addPostQuest(state,list){
        state.editsurvey.updated_data.postsurvey.push(list)
    },
    updatePreQuest(state,{index,list}){
        state.editsurvey.updated_data.presurvey[index]=list
    },
    updatePostQuest(state,{index,list}){
        state.editsurvey.updated_data.postsurvey[index]=list
    },
    changeEnablePreQuest(state, {index, qstate}) {
        state.editsurvey.updated_data.presurvey[index].general.enable = qstate;
    },
    changeEnablePostQuest(state, {index, qstate}) {
        state.editsurvey.updated_data.postsurvey[index].general.enable = qstate;
    },
    updateRational(state,{index,list}){
        state.editsurvey.updated_data.rational[index]=list
    },
    updateAlternativeDes(state,list){
        state.editsurvey.updated_data.rational.alternatives.content=list
    },
    updateAlternativeItems(state,items){
        state.editsurvey.updated_data.rational.alternatives.items=items
    },
    addAlt(state,list){
        state.editsurvey.updated_data.rational.alternatives.items.push(list)
    },
    updateAlt(state,{index,list}){
        state.editsurvey.updated_data.rational.alternatives.items[index]=list
    },
    rmAlt(state, index) {
        state.editsurvey.updated_data.rational.alternatives.items.splice(index, 1);
    },
    changeEnableAlt(state, {index, qstate}) {
        state.editsurvey.updated_data.rational.alternatives.items[index].enable = qstate;
    },
    updateObjectiveDes(state,list){
        state.editsurvey.updated_data.rational.objectives.content=list;
    },
    updateObjectiveItem(state,list){
        state.editsurvey.updated_data.rational.objectives.items=list;
    },
    updateObjectiveItemVar(state,{index,vari,val}){
        state.editsurvey.updated_data.rational.objectives.items[index][vari]=val;
    },
    updateObjectiveChildren(state,{index,indexc,vari,val}){
        state.editsurvey.updated_data.rational.objectives.items[index].children[indexc][vari]=val;
    },
    putPredicMat(state,mat){
        state.editsurvey.updated_data.rational.predicmat=mat
    },
    updatePredicMat(state,{altobj,val}){
        if(!state.editsurvey.updated_data.rational.predicmat) state.editsurvey.updated_data.rational.predicmat={}
        if(!state.editsurvey.updated_data.rational.predicmat[altobj.alt])state.editsurvey.updated_data.rational.predicmat[altobj.alt]={}
        state.editsurvey.updated_data.rational.predicmat[altobj.alt][altobj.obj]=val;
    },
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};

