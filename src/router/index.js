import Vue from 'vue'
import VueRouter from 'vue-router'

import store from "../store";

import { Notify } from 'quasar'

import Login from '@/views/Login'
import LayoutMain from '@/layouts/Main'
import AdminIndex from '@/views/AdminIndex'
import user from "./modules/user";
import token from "./modules/token";
import {Survey,EditSurvey} from "./modules/survey";



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { guest: true },
  },
  {
    path: '/main',
    name: 'LayoutMain',
    component: LayoutMain,
    meta: { requiresAuth: true },
    children: [
      { path: 'index.html', name:'AdminIndex',component: AdminIndex },
      { path: 'about',name: 'About',component: () => import('@/views/About.vue')},
      { path: 'help',name: 'AdminHelp',component: () => import('@/views/Help.vue')},
      { path: 'god',name: 'GodCtrl',component: () => import('@/views/GodControl.vue')},
      ]
  },
    user,
    token,
    Survey,
    EditSurvey,

]




const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters['auth/isAuthenticated']) {
      next();
      return;
    }
    Notify.create({
      type: 'warning',
      message: "You need to login"
    });
    next("/");
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    if (store.getters['auth/isAuthenticated']) {
      next("/main");
      return;
    }
    next();
  } else {
    next();
  }
});

export default router
