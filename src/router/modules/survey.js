import LayoutMain from "@/layouts/Main";
import LayoutEditSurvey from "@/layouts/EditSurvey";

import ListSurvey from "@/views/Surveys/ListSurvey";
import AddSurvey from "@/views/Surveys/AddSurvey";
import MainEdit from "@/views/Surveys/Edit/MainEditSurvey";
import LanguagesSurvey from "@/views/Surveys/LanguagesTranslationSurvey";
import GlobalOptSurvey from "@/views/Surveys/GlobalOptSurvey";
import LanguagesTemplateSurvey from "@/views/Surveys/LanguagesTemplateSurvey";
import LanguagesAddTranslation from "@/views/Surveys/LanguagesAddTranslation";
import LanguagesEditTranslation from "@/views/Surveys/LanguagesEditTranslation";
import WelcomePageES from "@/views/Surveys/Edit/WelcomePageES";
import PreSurvES from "@/views/Surveys/Edit/PreSurvES";
import PostSurvES from "@/views/Surveys/Edit/PostSurvES";
import TokensES from "@/views/Surveys/Edit/TokensES";
import ResultsES from "@/views/Surveys/Edit/ResultsES";
import AddPreSurvQuest from "@/views/Surveys/Edit/AddPreSurvQuest";
import EditPreSurvQuest from "@/views/Surveys/Edit/EditPreSurvQuest";
import AddPostSurvQuest from "@/views/Surveys/Edit/AddPostSurvQuest";
import EditPostSurvQuest from "@/views/Surveys/Edit/EditPostSurvQuest";
import RationalSurvES from "@/views/Surveys/Edit/RationalSurvES";
import AlternativeRSES from "@/views/Surveys/Edit/AlternativeRSES";
import ObjectivesRSES from "@/views/Surveys/Edit/ObjectivesRSES";
import AddAltRSES from "@/views/Surveys/Edit/AddAltRSES";
import EditAltRSES from "@/views/Surveys/Edit/EditAltRSES";
import PredicMatRSES from "@/views/Surveys/Edit/PredicMatRSES";
import EditTokenES from "@/views/Surveys/Edit/EditTokenES";
import AddTokenES from "@/views/Surveys/Edit/AddTokenES";
import BatchTokenES from "@/views/Surveys/Edit/BatchTokenES";

export const Survey={
    path: '/survey',
    component: LayoutMain,
    meta: { requiresAuth: true },
    children: [
        { path: 'list', name:'ListSurvey',component: ListSurvey },
        { path: 'add',name:'AddSurvey',component: AddSurvey},
        { path: 'languages',name:'LanguagesSurvey',component: LanguagesSurvey},
        { path: 'templates',name:'LanguagesTemplateSurvey',component: LanguagesTemplateSurvey},
        { path: 'languages/add',name:'AddLanguagesSurvey',component: LanguagesAddTranslation},
        { path: 'languages/edit/:language',name:'EditLanguagesSurvey',component: LanguagesEditTranslation},
        { path: 'globaloptions',name:'GlobalOptSurvey',component: GlobalOptSurvey},

    ]
};

export const EditSurvey={
    path: '/survey/edit/:id',
    component: LayoutEditSurvey,
    meta: { requiresAuth: true },
children: [
    { path: '/',name:'MainEditSurvey',component: MainEdit},
    { path: 'welcome',name:'WPEditSurvey',component: WelcomePageES},

    { path: 'presurvey',name:'PreSurvEditSurvey',component: PreSurvES},
    { path: 'presurvey/add',name:'AddPreSurvQuest',component: AddPreSurvQuest},
    { path: 'presurvey/edit/:qid',name:'EditPreSurvQuest',component: EditPreSurvQuest},
    { path: 'postsurvey',name:'PostSurvEditSurvey',component: PostSurvES},
    { path: 'postsurvey/add',name:'AddPostSurvQuest',component: AddPostSurvQuest},
    { path: 'postsurvey/edit/:qid',name:'EditPostSurvQuest',component: EditPostSurvQuest},

    { path: 'rationalsurvey',name:'RSEditSurvey',component: RationalSurvES},
    { path: 'rationalsurvey/alternatives',name:'AltRSEditSurvey',component: AlternativeRSES},
    { path: 'rationalsurvey/alternatives/add',name:'AddAltRSES',component: AddAltRSES},
    { path: 'rationalsurvey/alternatives/edit/:aid',name:'EditAltRSES',component: EditAltRSES},

    { path: 'rationalsurvey/objectives',name:'ObjRSEditSurvey',component: ObjectivesRSES},
    { path: 'rationalsurvey/prediction',name:'PreRSEditSurvey',component: PredicMatRSES},

    { path: 'tokens',name:'TokensEditSurvey',component: TokensES},
    { path: 'tokens/add',name:'AddTokenES',component: AddTokenES},
    { path: 'tokens/batch',name:'BatchTokenES',component: BatchTokenES},
    { path: 'tokens/edit/:tid',name:'EditTokenES',component: EditTokenES},

    { path: 'results',name:'ResultsEditSurvey',component: ResultsES},




]
}

