import LayoutMain from "@/layouts/Main";
import GlobalListToken from "@/views/Tokens/GlobalListToken";
import AddToken from "@/views/Tokens/AddToken";
import EditToken from "@/views/Tokens/EditToken";
import BatchToken from "@/views/Tokens/BatchToken";

export default {
    path: '/token',
    name: 'Token',
    component: LayoutMain,
    meta: { requiresAuth: true },
    children: [
        { path: 'list', name:'GlobalListToken',component: GlobalListToken },
        { path: 'add',name:'AddToken',component: AddToken},
        { path: 'batch',name:'BatchToken',component: BatchToken},
        { path: 'edit/:id',name:'EditToken',component: EditToken},
    ]
}

