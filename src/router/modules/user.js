import LayoutMain from "@/layouts/Main";
import ListAdminUser from "@/views/Users/ListAdminUser";
import AddAdminUser from "@/views/Users/AddAdminUser";
import EditAdminUser from "@/views/Users/EditAdminUser";

export default {
    path: '/adminuser',
    name: 'AdminUser',
    component: LayoutMain,
    meta: { requiresAuth: true },
    children: [
        { path: 'list', name:'ListAdminUser',component: ListAdminUser },
        { path: 'add',name:'AddAdminUser',component: AddAdminUser},
        { path: 'edit/:id',name:'EditAdminUser',component: EditAdminUser},
    ]
}

