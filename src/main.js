import Vue from 'vue'
import App from './App.vue'
import './quasar'
import router from './router'

import store from './store';

import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.use( CKEditor );



Vue.config.productionTip = false;

Vue.prototype.$DispDate = function (val,keeptime=false) {
  const locales='en-GB'
  const timezone='Europe/Zurich'
  if(val){
    let date=new Date(val);
    if (keeptime){
      return date.toLocaleString(locales, {timeZone: timezone})
    }else {
     return date.toLocaleString(locales, {timeZone: timezone,year: "numeric", month: "numeric", day: "numeric"})
    }}
};


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
