import axios from "axios";
import store from "../store";
import {Notify} from 'quasar'

axios.defaults.baseURL = process.env.VUE_APP_API_URL

axios.interceptors.request.use(
    (config) => {

        if (store.getters['auth/isAuthenticated']) {
            const token = localStorage.getItem('eawag-token-admin');
            config.headers['Authorization'] = `Bearer ${ token }`;
        }

        return config;
    },

    (error) => {
        if (error.response.status === 401) {
            store.dispatch('auth/LogOut');
            return {status:"Error",message:error.message}

        }
        return Promise.reject(error);
    }
);
axios.interceptors.response.use(
    function(response) {
        if(process.env.NODE_ENV !== 'production') console.log(JSON.parse(JSON.stringify(response)))
        if(response.headers['content-type']==="application/octet-stream; charset=utf-8" || response.headers['content-type']==="application/octet-stream") return response
        const payload=response.data;

        //two next conditions for image upload compatibiity
        if(payload.url){
            Notify.create({
                type: 'info',
                message: "file uploaded"
            })
            return payload
        } 
        if(payload.error){
            Notify.create({
                type: 'warning',
                message: payload.error
            })
            return payload
        }
        if (payload.status==="OK"){
            return payload
        } else {
            Notify.create({
                type: 'warning',
                message: payload.message
            })
            return payload
        }
        },
    function(error) {
        if (error.response.status === 401) {
            store.dispatch('auth/LogOut');
            return {status:"Error",message:error.message}

        }
        // handle error
            Notify.create({
                type: 'negative',
                message: "Error with the api :" +error.message
            });
            //
        return {status:"Error",message:error.message}
    });
// const handleError = fn => (...params) =>
//     fn(...params).catch(error => {
// // vm.flash(`${error.response.status}: ${error.response.statusText}`, 'error');
//     console.log("Error api :",error);
// }    );

export const api = {
    // User
    isexist:async () => {
        const payload= await axios.get('adminusers/exist');
        return payload.adminpresent
    },
    createfirstuser: async form => {
        return await axios.post('adminusers/createfirst', form);
    },
    loginuser: async payload => {
        return await axios.post('adminusers/login', payload);
    },
    listuser: async () => {
        return await axios.get('adminusers/all');
    },
    registeruser: async form => {
        return await axios.post('adminusers/register', form);
    },
    getuser: async (id) => {
        return await axios.get('adminusers/user/' + id);
    },
    updateuser: async (form,id) => {
        return await axios.put('adminusers/user/'+id, form);
    },
    enableuser: async (id) => {
        return await axios.put('adminusers/user/'+id, {enable:true});
    },
    disableuser: async (id) => {
        return await axios.put('adminusers/user/'+id, {enable:false});
    },
    removeuser: async (id) => {
        return await axios.delete('adminusers/user/'+id);
    },
    // Token
    listtoken: async () => {
        return await axios.get('tokens/all');
    },
    enabletoken: async (id) => {
        return await axios.put('tokens/token/'+id, {enable:true});
    },
    disabletoken: async (id) => {
        return await axios.put('tokens/token/'+id, {enable:false});
    },
    removetoken: async (id) => {
        return await axios.delete('tokens/token/'+id);

    },
    registertoken: async form => {
        return await axios.post('tokens/register', form);
    },
    gettoken: async (id) => {
        return await axios.get('tokens/token/' + id);
    },
    updatetoken: async (form,id) => {
        return await axios.put('tokens/token/'+id, form);
    },
    listsurveytoken: async (id) => {
    return await axios.get('tokens/survey/'+id);
    },
    testsurveytoken: async (id) => {
        return await axios.get('tokens/survey/'+id+'/test');
    },
    downloadtokenJSON: async (id,short_name) => {
        return await axios.get('tokens/downjson/' + id,{
            responseType: 'blob'
        }).then((response) => {
            const data = new Blob([response.data])
            if (data) {
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(
                    data
                );
                link.setAttribute('download', short_name + '-answer.json');
                document.body.appendChild(link);
                link.click();
            }

        });
    },
    downloadtokenCSV: async (id,short_name) => {
        return await axios.get('tokens/downcsv/' + id,{
            responseType: 'blob'
        }).then((response) => {
            const data = new Blob([response.data])
            if (data) {
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(
                    data
                );
                link.setAttribute('download', short_name + '-answer.csv');
                document.body.appendChild(link);
                link.click();
            }
        });
    },
    //Surveys
    dropsurvey: async () => {
        return await axios.get('surveys/drop');
    },

    listsurvey: async () => {
        return await axios.get('surveys/all');
    },
    enablesurvey: async (id) => {
        return await axios.put('surveys/survey/enable/'+id,{} );
    },
    disablesurvey: async (id) => {
        return await axios.put('surveys/survey/disable/'+id,{});
    },
    removesurvey: async (id) => {
        return await axios.delete('surveys/survey/'+id);

    },
    createsurvey: async form => {
        return await axios.post('surveys/create', form);
    },
    getsurvey: async (id) => {
        return await axios.get('surveys/survey/' + id);
    },
    updatesurvey: async (form,id) => {
        return await axios.put('surveys/survey/'+id, form);
    },
    downloadsurvey: async (id,short_name) => {
        return await axios.get('surveys/downsurvey/' + id,{
            responseType: 'blob'
        }).then((response) => {
            const data = new Blob([response.data])
            if (data) {
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(
                    data
                );
                link.setAttribute('download', short_name + '.zip');
                document.body.appendChild(link);
                link.click();
            }
        });
    },


    uploadsurvey: async(file,newid)=>{
        var formData = new FormData();
        formData.append("upload", file);
        if(newid){
            formData.append("newid",newid)
        }
        return await axios.post('surveys/upload', formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        });
    },
    clonesurvey: async (id,newid) => {
        var formData = new FormData();
        formData.append("srcsn", id);
        formData.append("newsn", newid);
        return await axios.post('surveys/clone', formData);
    },

    uploadpicture: async(file,id,lang,objname,objname2=null)=>{
        var formData = new FormData();
        formData.append("image", file);
        let url = 'surveys/survey/'+id+'/picture/'+lang+'/'+objname;
        if (objname2) {
            url = url +'/'+objname2
        }
        return await axios.post(url, formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
        });

    },



    savetranslation: async () => {
        return await axios.get('surveys/savelangtrans');
    },

    loadtranslation: async () => {
        return await axios.get('surveys/loadlangtrans');
    },
    downloadtranslation: async () => {
        return await axios.get('surveys/downlangtrans',{
            responseType: 'blob'
        }).then((response) => {
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(
                new Blob([response.data])
            );
            link.setAttribute('download', 'LanguageTrans.zip');
            document.body.appendChild(link);
            link.click();
        });
    },
    uploadtranslation: async (form) => {
        return await axios.post('surveys/uploadlangtrans', form,{
            headers: {'Content-Type': 'multipart/form-data'}});
    },
    listtranslation: async () => {
        return await axios.get('surveys/langtrans');
    },
    gettranslation: async (language) => {
        return await axios.get('surveys/langtrans/'+language);
    },
    createtranslation: async (form) => {
        return await axios.post('surveys/langtrans',form);
    },
    removetranslation: async (id) => {
        return await axios.delete('surveys/langtrans/'+id)
    },
    updatetranslation: async (form,id) => {
        return await axios.put('surveys/langtrans/'+id, form);
    },
    createtemplatevar: async (form) => {
        return await axios.post('surveys/langdef',form);
    },
    updatetemplatevar: async (form,id) => {
        return await axios.put('surveys/langdef/'+id, form);
    },
    removetemplatevar: async (id) => {
        return await axios.delete('surveys/langdef/'+id);
    },
    listtemplatevar: async () => {
    return await axios.get('surveys/langdef');
    },
    savetemplatevar: async () => {
        return await axios.get('surveys/savelangdef');
    },

    loadtemplatevar: async () => {
        return await axios.get('surveys/loadlangdef');
    },
    downloadtemplatevar: async () => {
        return await axios.get('surveys/downlangdef',{
            responseType: 'blob'
        }).then((response) => {
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(
                new Blob([response.data])
            );
            link.setAttribute('download', 'LanguageDef.json');
            document.body.appendChild(link);
            link.click();
        });
    },
    uploadtemplatevar: async (form) => {
        return await axios.post('surveys/uploadlangdef', form,{
            headers: {'Content-Type': 'multipart/form-data'}});
    },

    listsurveyglobalopt: async () => {
        return await axios.get('surveys/options');
    },
    updatesurveyglobalopt: async (form) => {
        return await axios.post('surveys/options', form);
    },
}
