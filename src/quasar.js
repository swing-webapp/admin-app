import Vue from 'vue'

import './styles/quasar.sass'
import '@quasar/extras/material-icons/material-icons.css'
import { Quasar,Notify,Dialog,Loading} from 'quasar'

Vue.use(Quasar, {
  config: {
      notify: { /* look at QUASARCONFOPTIONS from the API card (bottom of page) */ },
      loading:{},
  },
  plugins: {Notify,Dialog,Loading
  }
 })
